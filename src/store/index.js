import { createStore } from "vuex";

export default createStore({
  state: {
    title: "Vuex Store",
    notes: [],
  },
  getters: {
    totalNotes(state) {
      console.log("state", state.notes.length);
      return state.notes.length;
    },
  },
  //create function
  mutations: {
    SAVE_NOTE(state, title) {
      console.log("SAVE_NOTE:store");
      state.notes.push(title);
    },
  },
  //actions use fun at mutations
  actions: {
    saveNote({ commit }, title) {
      console.log("saveNote:store");
      commit("SAVE_NOTE", title);
    },
  },
  modules: {},
});
